const rp = require('request-promise');
const cheerio = require('cheerio');
const BPromise = require('bluebird');
const fs = require('fs');
const HttpsProxyAgent = require('https-proxy-agent');
const SocksProxyAgent = require('socks-proxy-agent');
const childProcess = require('child_process');

class Raw {

  checkProxy(proxy) {
    return new BPromise((resolve, reject) => {
      console.log(`Checking proxy ${proxy.ip} ${proxy.port} ${proxy.type} ...`);
      const commonRequestCheck = (sock, type) => {
        return new BPromise((resolve, reject) => {
          // let agentCurrent;
          // if (sock) {
          //   agentCurrent = new SocksProxyAgent(`${type}://${proxy.ip}:${proxy.port}`)
          // } else {
          //   agentCurrent = new HttpsProxyAgent(`${type}://${proxy.ip}:${proxy.port}`)
          // }
          console.log(`Type ${type} checking ...`);
          return childProcess.exec(`curl --proxy ${type}://${proxy.ip}:${proxy.port} http://ip-api.com/json`, (error, stdout, stderr) => {
            console.log('Check completed');
            if (error) {
              console.log('STATUS: fail');
              return reject(false);
            }
            const reg = new RegExp(`${proxy.ip}`, 'g');
            if (reg.test(stdout.toString())) {
              console.log('STATUS: live');
              return resolve(true);
            } else {
              console.log('STATUS: fail');
              return reject(false);
            }
          });
          // return rp({
          //   uri: 'http://vivu.nhutuit.com',
          //   method: 'GET',
          //   agent: agentCurrent,
          //   timeout: 10000,
          //   followRedirect: true,
          //   maxRedirects: 10
          // }).then(data => {
          //   console.log('Check completed');
          //   if (/<!DOCTYPE html>/g.test(data.toString())) {
          //     console.log('STATUS: live');
          //     return resolve(true);
          //   } else {
          //     console.log('STATUS: fail');
          //     return reject(false);
          //   }
          // }).catch(err => {
          //   console.log('STATUS: fail');
          //   return reject(false);
          // });
        })
      };
      if (/^socks/g.test(proxy.type)) {
        return commonRequestCheck(true, proxy.type).then(data => {
          return resolve(proxy);
        }).catch(err => {
          return reject(false);
        });
      }
      return commonRequestCheck(false, 'http').then(data => {
        proxy.type = 'http';
        return resolve(proxy);
      }).catch(err => {
        return commonRequestCheck(true, 'socks5').then(data => {
          proxy.type = 'socks5';
          return resolve(proxy);
        }).catch(err => {
          return commonRequestCheck(true, 'socks4').then(data => {
            proxy.type = 'socks4';
            return resolve(proxy);
          }).catch(err => {
            return commonRequestCheck(false, 'https').then(data => {
              proxy.type = 'https';
              return resolve(proxy);
            }).catch(err => {
              return reject(false);
            });
          });
        })
      });

    });
  }

  run() {

    const listProxyLive = [],
      listProxyFail = [],
      date = new Date(),
      strDateCurrent = `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`,
      directoryCurrent = process.cwd(),
      willPathMkdir = `${directoryCurrent}/${strDateCurrent}`;
    fs.mkdir(willPathMkdir, (err) => {

      const loggerLive = fs.createWriteStream(`${willPathMkdir}/log-live.txt`, {
          flags: 'w'
        }),
        loggerFail = fs.createWriteStream(`${willPathMkdir}/log-fail.txt`, {
          flags: 'w'
        }),
        loggerHistory = fs.createWriteStream('history.txt', {
          flags: 'a'
        }),
        urlGet = (page) => {
          return new BPromise((resolve, reject) => {
            console.log('Start load page:');
            console.log(`http://www.xroxy.com/proxylist.php?desc=true&pnum=${page}`);
            return rp({
              uri: `http://www.xroxy.com/proxylist.php?desc=true&pnum=${page}`,
              method: 'GET'
            }).then(response => {
              const $ = cheerio.load(response),
                table = $('#content table').eq(1).find('tbody tr');
              console.log('Loaded page ...');
              let listProxyCurrent = [];
              const tableEach = () => {
                return new BPromise((resolve1, reject1) => {
                  const lengthTable = table.length;
                  table.each((i, elem) => {
                    const getValue = (position) => {
                      return $(elem).find('td').eq(position).find('a').text();
                    }
                    const ipCurrent = getValue(1).trim();
                    if (ipCurrent !== '') {
                      const port = getValue(2).trim(),
                        type = getValue(3).trim().toLowerCase(),
                        proxyCurrent = {
                          ip: ipCurrent,
                          port: port,
                          type: type
                        };

                      listProxyCurrent.push(proxyCurrent);
                    }
                    if (i === lengthTable - 1) {
                      return resolve1(true);
                    }
                  });
                });

              };
              return tableEach().then(() => {
                const length = listProxyCurrent.length,
                  logSuccess = (index, eachProxy, e) => {
                    console.log(`${index} ${e.ip} ${e.port} ${e.type} ${e.status}`);
                    console.log('__________________________________________________');
                    if (e.status === 'live') {
                      listProxyLive.push(e);
                      loggerLive.write(`${e.ip} ${e.port} ${e.type} ${e.status}\r\n`);
                    } else {
                      listProxyFail.push(e);
                      loggerFail.write(`${e.ip} ${e.port} ${e.type} ${e.status}\r\n`);
                    }
                    if (index === length - 1) {
                      return resolve(true);
                    }
                    return eachProxy(++index);
                  },
                  eachProxy = (index) => {
                    let e = listProxyCurrent[index];
                    this.checkProxy(e).then(check => {
                      e.status = 'live';
                      return logSuccess(index, eachProxy, check);
                    }).catch(err => {
                      e.status = 'fail';
                      return logSuccess(index, eachProxy, e);
                    });
                  };
                console.log(`Total ${length} proxy need handle`);
                return eachProxy(0);
              });

            }).catch(err => {
              return reject(err);
            })
          });

        },
        listPromise = [];

      const forEachTimeOut = (index) => {
        if (50 > index) {
          if (index === 0) {
            urlGet(index).then(data => {
              console.log('Completed ', index);
              forEachTimeOut(++index);
            }).catch(err => {
              console.error(err);
            });
          } else {
            console.log('Waiting ...');
            setTimeout(() => {
              urlGet(index).then(data => {
                console.log('Completed ', index);
                forEachTimeOut(++index);
              }).catch(err => {
                console.error(err);
              });
            }, 5000);
          }
        } else {
          // listProxy.forEach(e => {
          //   logger.write(`${e.ip} ${e.port} ${e.type} ${e.status}\r\n`);
          // });
          console.log('Xong nhé');
          const atTime = `At time ${strDateCurrent} get proxy analytics:`,
            countLive = `Count proxy live: ${listProxyLive.length}`,
            countFail = `Count proxy fail: ${listProxyFail.length}`;
          loggerHistory.write('-------------------------------------------\r\n');
          loggerHistory.write(`${atTime}\r\n`);
          loggerHistory.write(`${countLive}\r\n`);
          loggerHistory.write(`${countFail}\r\n`);
          loggerHistory.write('-------------------------------------------\r\n');
          console.log(atTime);
          console.log(countLive);
          console.log(countFail);
        }
      };

      forEachTimeOut(0);

      // return BPromise.all(listPromise).then(data => {

      //   listProxy.forEach(e => {
      //     logger.write(`${e.ip} ${e.port} \r\n`);
      //   });
      //   console.log(`Get proxy successfully`);

      // }).catch(err => {
      //   console.error(err);
      // });

    });

  }

}

const raw = new Raw(),
  CronJob = require('cron').CronJob;
console.log('Start tool get proxy by NhutDev');
const job = new CronJob({
  cronTime: '00 0 1 * * *',
  onTick: () => {
    return raw.run();
  },
  start: true,
  timeZone: 'Asia/Ho_Chi_Minh'
});
// const commonRequestCheck = (sock, type, proxy) => {
//   return new BPromise((resolve, reject) => {
//     let agentCurrent;
//     if (sock) {
//       agentCurrent = new SocksProxyAgent(`${type}://${proxy.ip}:${proxy.port}`)
//     } else {
//       agentCurrent = new HttpsProxyAgent(`${type}://${proxy.ip}:${proxy.port}`)
//     }
//     console.log(`Type ${type} checking ...`);
//     console.log(agentCurrent);
//     return rp({
//       uri: 'http://vivu.nhutuit.com',
//       method: 'GET',
//       agent: agentCurrent,
//       timeout: 10000,
//       followRedirect: true,
//       maxRedirects: 10
//     }).then(data => {
//       console.log('Check completed');
//       if (/<!DOCTYPE html>/g.test(data.toString())) {
//         console.log('STATUS: live');
//         return resolve(true);
//       } else {
//         console.log('STATUS: fail');
//         return reject(false);
//       }
//     }).catch(err => {
//       console.log('STATUS: fail');
//       return reject(false);
//     });
//   })
// };

// commonRequestCheck(false, 'http', {
//   ip: '123.207.170.57',
//   port: 808
// }).then(data => {
//   console.log(data);
// }).catch(err => {
//   console.log(err);
// });